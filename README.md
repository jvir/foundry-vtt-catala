# FOUNDRY VTT CATALÀ

Complete Translation to Catalonian of the Foundry Virtual Tabletop.

Author: David Montilla  [Discord: Montver#9776] / Dr.Slump#7725 / Josep Torra

Foundry VTT compatibility: 0.9
Foundry VTT minimum Core Version" : 0.9

Version: 0.9.1

**English**

This module allows to choose the Catalonian in FoundryVTT.
If you find any errors or have any recommendations please send me a message to Montver # 9776 in Discord.

Installation
In the 'Add-On Modules' tab of the main menu, click on 'Install Module' and write down this to the pop-up window:
https://gitlab.com/montver/foundry-vtt-catala/-/raw/master/ca/module.json
If that doesn't work, you could try downloading the file ca.zip and unzip it in the 'Data\modules' folder and rename it to FoundryVTT-CAT.
Also, you have to activate the module in your world, and then choose the language catalonian from the dropdown menu in the general settings.

**Català**

Aquest mòdul permet traduir al català l'interfície de FoundryVTT.
Si trobes cap error o tens cap recomanació ens pots fer arribar un missatge a Discord a Montver#9776 o Dr.Slump#7725.

Instalació:

A l'opció de "Add-On Modules" en el menú principal del programa, fes clic a "Install Module" i escriu el següent:
https://gitlab.com/montver/foundry-vtt-catala/-/raw/master/ca/module.json
Si no funciona, pots intentar descarregar l'arxiu ca.zip i extreura el seu contingut a la carpeta "Data\modules" i reanomenar el directori a FoundryVTT-CAT.
Un cop fet hauràs d'habilitar el mòdul dins del teu món de joc i canviar l'idioma general de la configuració general del programa.

**Acknowledgements**

Thanks to Simone Ricciardi for the base files, and Cosmo Corban/Jose Lozano for the spanish base files
